package com.boomi.connector.smb;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.util.BaseConnection;
import com.boomi.util.StringUtil;
import com.hierynomus.protocol.commons.IOUtils;
import com.hierynomus.smbj.SMBClient;
import com.hierynomus.smbj.SmbConfig;
import com.hierynomus.smbj.auth.AuthenticationContext;
import com.hierynomus.smbj.connection.Connection;
import com.hierynomus.smbj.session.Session;
import com.hierynomus.smbj.share.DiskShare;

public class WindowsSMBFileConnection extends BaseConnection {
    public enum ConnectionProperties {HOSTNAME, PORT, USERNAME, PASSWORD, DOMAIN, TIMEOUT, SOTIMEOUT, SHARENAME}
    PropertyMap _connProps;
    Session _session = null;
    SMBClient _client = null;
	DiskShare _share=null;
	Logger _logger = Logger.getLogger(WindowsSMBFileConnection.class.getName());
	public WindowsSMBFileConnection(BrowseContext context) {
		super(context);
		_connProps=context.getConnectionProperties();
	}	
	
	DiskShare connect()
	{
		SmbConfig config = SmbConfig.builder()
					.withDfsEnabled(true)
		            .withTimeout(_connProps.getLongProperty(ConnectionProperties.TIMEOUT.name(), 60L), TimeUnit.SECONDS) // Timeout sets Read, Write, and Transact timeouts (default is 60 seconds)
		            .withSoTimeout(_connProps.getLongProperty(ConnectionProperties.SOTIMEOUT.name(), 0L), TimeUnit.SECONDS) // Socket Timeout (default is 0 seconds, blocks forever)
		            .build();

	    _client = new SMBClient(config);

	    String hostName = _connProps.getProperty(ConnectionProperties.HOSTNAME.name());
	    if (StringUtil.isEmpty(hostName))
	    	throw new ConnectorException("Host name is required");
	    String username = _connProps.getProperty(ConnectionProperties.USERNAME.name());
	    if (StringUtil.isEmpty(username))
	    	throw new ConnectorException("Username is required");
	    String password = _connProps.getProperty(ConnectionProperties.PASSWORD.name());
	    if (StringUtil.isEmpty(password))
	    	throw new ConnectorException("Password is required");
	    String domain = _connProps.getProperty(ConnectionProperties.DOMAIN.name());
	    if (StringUtil.isEmpty(domain))
	    	throw new ConnectorException("Domain is required");
	    String shareName = _connProps.getProperty(ConnectionProperties.SHARENAME.name());
	    if (StringUtil.isEmpty(shareName))
	    	throw new ConnectorException("Share name is required");
	    try {
	    	Connection connection = _client.connect(hostName);
	        AuthenticationContext ac = new AuthenticationContext(username, password.toCharArray(), domain);
	        Session session = connection.authenticate(ac);
	        if (session==null)
	        	throw new ConnectorException("Problem starting session");
			_share = (DiskShare) session.connectShare(shareName);
	        if (_share==null)
	        	throw new ConnectorException("Problem accessing share:" + shareName);
	    } catch (IOException e) {
	    	close();
			throw new ConnectorException(e);
		}
		return _share;
	}
	
	protected void close()
	{
		IOUtils.closeQuietly(_share);
		IOUtils.closeQuietly(_session);
		IOUtils.closeQuietly(_client);
	}

	protected void testConnection() {
		_share = this.connect();
		close();
	}

	public void setLogger(Logger logger) {
		this._logger = logger;		
	}
}